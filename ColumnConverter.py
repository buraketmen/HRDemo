import pandas as pd
import numpy as np
import math

def satisfaction_level(x):
    if x<=0.20:
        return " Very Dissatisfied"
    if x<=0.40:
        return "Dissatisfied"
    if x<=0.60:
        return "Neutral"
    if x<=0.80:
        return "Satisfied"
    if x<=1.0:
        return "Very Satisfied"

def last_evaluation(x):
    if x<=0.50:
        return "Bad"
    if x<=1:
        return "Good"

def average_montly_hours(x):
    if x<=100:
        return " 90-100"
    if x<=120:
        return "100-120"
    if x<=140:
        return "120-140"
    if x<=160:
        return "140-160"
    if x<=180:
        return "160-180"
    if x<=200:
        return "180-200"
    if x<=220:
        return "200-220"
    if x<=240:
        return "220-240"
    if x<=260:
        return "240-260"
    if x<=280:
        return "260-280"
    if x<=300:
        return "280-300"
    if x<=500:
        return "300-320"


