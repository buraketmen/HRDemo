import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import ColumnConverter as CC
import warnings
warnings.filterwarnings("ignore")
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

columns= ['empid', 'satisfaction_level', 'last_evaluation', 'number_project', 'average_montly_hours', 'time_spend_company', 'Work_accident', 'left', 'promotion_last_5years', 'dept', 'salary']
outliers_columns= ['number_project','average_montly_hours','time_spend_company']
columns_withoutlabel = ['satisfaction_level', 'last_evaluation', 'number_project', 'average_montly_hours', 'time_spend_company', 'Work_accident', 'promotion_last_5years', 'dept', 'salary']
object_columns = ['dept','salary']
target = 'left'

hrData = pd.read_csv('HR-dataset.csv')
hrData.dropna(how='all', inplace=True)
hrData.fillna(hrData.mean(), inplace=True)
hrData = hrData[(hrData.satisfaction_level <= 1.0) & (hrData.satisfaction_level >= 0)]
hrData = hrData[(hrData.last_evaluation <= 1.0) & (hrData.last_evaluation >= 0)]
hrData_for_plot = hrData.copy()
hrData[target] = hrData[target].map({1: True, 0: False}).astype(bool)


hrData_for_plot.rename(columns={'satisfaction_level':'Satisfaction Level', 'last_evaluation':'Last Evaluation',
                                'number_project':'Number Project', 'average_montly_hours':'Average Montly Hours',
                                'time_spend_company':'Time Spend Company (Year)', 'Work_accident':'Work Accident',
                                'promotion_last_5years':'Promotion Last 5 Years', 'dept':'Department', 'salary':'Salary','left':'left'},inplace=True)
plt.figure(1)
figbox, a = plt.subplots(1, 2, figsize=(16, 8))
sns.boxplot(y = hrData_for_plot['Time Spend Company (Year)'],ax=a[0] )
sns.boxplot(y = hrData_for_plot['Average Montly Hours'], ax=a[1])
figbox.savefig("./Graph/Box Plot.png")

hrData_for_plot[target] = hrData_for_plot[target].map({1: "Yes", 0:"No"})
hrData_for_plot["Promotion Last 5 Years"] = hrData_for_plot["Promotion Last 5 Years"].map({1: "Yes", 0: "No"})
hrData_for_plot["Work Accident"] = hrData_for_plot["Work Accident"].map({1: "Yes", 0: "No"})
hrData_for_plot['Department'] = hrData_for_plot["Department"].map({'sales':'Sales', 'accounting':'Accounting',
            'hr':'Human Resources','technical':'Technical', 'support':'Support', 'management':'Management', 'IT':'IT',
            'product_mng':'Product Manager', 'marketing':'Marketing', 'RandD':'Research & Dev.'})
hrData_for_plot['Satisfaction Level'] = hrData_for_plot['Satisfaction Level'].apply(CC.satisfaction_level)
hrData_for_plot['Last Evaluation'] = hrData_for_plot['Last Evaluation'].apply(CC.last_evaluation)
hrData_for_plot['Average Montly Hours'] = hrData_for_plot['Average Montly Hours'].apply(CC.average_montly_hours)

plt.figure(2)
hrData_for_plot.groupby(["Average Montly Hours", target]).size().unstack().plot(kind='bar', stacked=True, figsize=(20,10)).set_ylabel('Count')
plt.tight_layout()
plt.savefig("./Graph/Average Montly Hours - Left.png")

plt.figure(3)
plt.tight_layout()
figbar, axes = plt.subplots(3, 3, figsize=(15, 15))
y = hrData_for_plot[target].value_counts()
hrData_for_plot.groupby([target]).size().plot(kind='bar', ax=axes[0, 0]).set_ylabel('Count')
hrData_for_plot.groupby(["Department", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[0, 1])
hrData_for_plot.groupby(["Promotion Last 5 Years", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[0, 2])
hrData_for_plot.groupby(["Work Accident", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[1, 0]).set_ylabel('Count')
hrData_for_plot.groupby(["Time Spend Company (Year)", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[1, 1])
hrData_for_plot.groupby(["Salary", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[1, 2])
hrData_for_plot.groupby(["Number Project", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[2, 0]).set_ylabel('Count')
hrData_for_plot.groupby(["Last Evaluation", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[2, 1])
hrData_for_plot.groupby(["Satisfaction Level", target]).size().unstack().plot(kind='bar', stacked=True, ax=axes[2, 2])
plt.tight_layout()
figbar.savefig('./Graph/About Left.png')

plt.figure(4)
figpie, axes = plt.subplots(3, 3, figsize=(15, 15))
hrData_for_plot.groupby([target]).size().plot(kind='pie', stacked=True, ax=axes[0, 0],title="Left",autopct='%1.0f%%').set_ylabel('')
hrData_for_plot.groupby(["Department"]).size().plot(kind='pie', stacked=True, ax=axes[0, 1],title="Department",autopct='%1.0f%%', pctdistance=1.1, labeldistance=1.25).set_ylabel('')
hrData_for_plot.groupby(["Promotion Last 5 Years"]).size().plot(kind='pie', stacked=True, ax=axes[0, 2],title="Promotion Last 5 Years",autopct='%1.0f%%').set_ylabel('')
hrData_for_plot.groupby(["Work Accident"]).size().plot(kind='pie', stacked=True, ax=axes[1, 0],title="Work Accident",autopct='%1.0f%%').set_ylabel('')
hrData_for_plot.groupby(["Time Spend Company (Year)"]).size().plot(kind='pie', stacked=True, ax=axes[1, 1],title="Time Spend Company (Year)",autopct='%1.0f%%', pctdistance=1.1, labeldistance=1.3).set_ylabel('')
hrData_for_plot.groupby(["Salary"]).size().plot(kind='pie', stacked=True, ax=axes[1, 2],title="Salary",autopct='%1.0f%%').set_ylabel('')
hrData_for_plot.groupby(["Number Project"]).size().plot(kind='pie', stacked=True, ax=axes[2, 0],title="Number Project",autopct='%1.0f%%').set_ylabel('')
hrData_for_plot.groupby(["Last Evaluation"]).size().plot(kind='pie', stacked=True, ax=axes[2, 1],title="Last Evaluation",autopct='%1.0f%%').set_ylabel('')
hrData_for_plot.groupby(["Satisfaction Level"]).size().plot(kind='pie', stacked=True, ax=axes[2, 2],title="Satisfaction Level",autopct='%1.0f%%').set_ylabel('')
plt.tight_layout()
figpie.savefig('./Graph/About Features.png')