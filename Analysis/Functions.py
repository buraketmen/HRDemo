# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
import warnings
import os
import pickle
import json
import numpy as np
import pandas as pd
from scipy import stats
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import make_scorer, f1_score, auc, roc_curve, confusion_matrix, matthews_corrcoef, accuracy_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.feature_selection import RFE, chi2, SelectKBest, VarianceThreshold
from sklearn.neural_network import MLPClassifier
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from matplotlib import rcParams
import scikitplot as skplt
import seaborn as sns


warnings.filterwarnings("ignore")

basepath = os.path.dirname(os.path.realpath(__file__))

SEED = np.random.seed(100)
def data_manipulation(df, OUTLIER=False,SCALING=False):
    create_directory()
    df = drop_na_values(df)
    if OUTLIER:
        df = drop_outlier(df)
    df = nominal_to_numeric(df)
    if SCALING:
        df = scale_data(df)
    return df

def drop_na_values(df):
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    df.dropna(how='all', inplace=True)
    numeric_df = df.select_dtypes(include=numerics)
    numeric_columns = numeric_df.columns
    for column in numeric_columns:
        df[column].fillna(df[column].mean(), inplace=True)
    df.dropna(how="any", inplace=True)
    return df

def remove_outlier(df, col_name):
    q1 = df[col_name].quantile(0.15)
    q3 = df[col_name].quantile(0.85)
    iqr = q3-q1
    fence_low = q1-1.5*iqr
    fence_high = q3+1.5*iqr
    df = df.loc[(df[col_name] > fence_low) & (df[col_name] < fence_high)]
    return df

def drop_outlier(df):
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    numeric_df = df.select_dtypes(include=numerics)
    numeric_columns = numeric_df.columns
    for column in numeric_columns:
        df = remove_outlier(df, column)
    return df

def nominal_to_numeric(df):
    object = ['object','category']
    object_df = df.select_dtypes(include=object)
    object_columns = object_df.columns
    for col in object_columns:
        state_list = df[col].unique().tolist()
        state_mapping = dict(zip(state_list, range(len(state_list))))
        df.replace({col: state_mapping}, inplace=True)
    return df

def scale_data(df):
    #0-1
    min_max_scaler = preprocessing.MinMaxScaler()
    newdata = df.values
    newcolumns = df.columns
    newdata_scaled = min_max_scaler.fit_transform(newdata)
    df = pd.DataFrame(data=newdata_scaled, columns=newcolumns)
    return df

def create_directory():
    if not os.path.exists(os.path.join(basepath, "Plots")):
        os.mkdir(os.path.join(basepath, "Plots"))
    if not os.path.exists(os.path.join(basepath, "Models")):
        os.mkdir(os.path.join(basepath, "Models"))
    if not os.path.exists(os.path.join(basepath, "ML_params")):
        os.mkdir(os.path.join(basepath, "ML_params"))

def draw_corr_matrix(df):
    plt.figure(1, figsize=(15, 12))
    corr = df.corr()
    sns.heatmap(corr, annot=True)
    plt.tight_layout()
    plt.savefig(basepath+"/Plots/Heatmap.png")

def draw_roc_curve_micro(classifier, legend, X_test, y_test):
    try:
        y_test_roc = np.array([([0, 1] if y else [1, 0]) for y in y_test])
        y_score = classifier.predict_proba(X_test)
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(2):
            fpr[i], tpr[i], _ = roc_curve(y_test_roc[:, i], y_score[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test_roc.ravel(), y_score.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        plt.figure(2)
        plt.plot(fpr["micro"], tpr["micro"], label=legend + "(AUC = %0.4f)" % roc_auc["micro"])
        plt.title("ROC Curve")
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.plot([0, 1], [0, 1], linestyle='--')
        plt.legend()
        return roc_auc["micro"]
    except Exception:
        return 0

def draw_roc_curve_macro(algorithm_name, y_test, pred):
    try:
        fpr, tpr, _ = roc_curve(y_test, pred)
        roc_auc_score = auc(fpr, tpr)
        plt.figure(2)
        plt.plot(fpr, tpr, label=algorithm_name + "(AUC = %0.4f)" % roc_auc_score)
        plt.title("ROC Curve")
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.plot([0, 1], [0, 1], linestyle='--')
        plt.legend()
        return roc_auc_score
    except Exception:
        return 0

def draw_feature_importance(algcoef, features, modelname, fignumber):
    coef = pd.Series(algcoef, index=features)
    imp_coef = coef.sort_values()
    plt.figure(fignumber)
    plt.title("Feature Importance ({})".format(modelname))
    rcParams['figure.figsize'] = (8.0, 8.0)
    if modelname == "Linear Regression":
        plt.title("Feature Importance ({}) \n (RED: Negative | BLUE: Positive) ".format(modelname))
        colors = {}
        sortedcolors = {}
        for col in imp_coef.index:
            if imp_coef[col] < 0:
                colors[col] = 'r'
            else:
                colors[col] = 'b'
        imp_coef = abs(imp_coef)
        imp_coef = imp_coef.sort_values()
        for index in imp_coef.index:
            sortedcolors[index] = colors.get(index)
        imp_coef.plot(kind="barh", color=sortedcolors.values())
    else:
        imp_coef.plot(kind="barh")

    plt.tight_layout()
    plt.savefig(basepath+ "/Plots/Feature Importance Using {}".format(modelname))

def draw_conf_matrix(y_test, pred, algorithm, fignumber):
    labels = y_test.value_counts().sort_index().index.tolist()
    conf_matrix = confusion_matrix(y_test, pred)
    plt.figure(fignumber, figsize=(15, 15))
    plt.title('{} Confusion Matrix'.format(algorithm))
    plot = sns.heatmap(conf_matrix, annot=True, fmt='d')
    plot.set_xticklabels(labels, rotation='vertical')
    plot.set_yticklabels(labels, rotation='horizontal')
    plot.set_ylabel('Act')
    plot.set_xlabel('Pred')
    plot.figure.savefig(basepath+'/Plots/Confusion Matrix-{}.png'.format(algorithm))

def feature_selection_with_logistic(df, features, target):
    X = df[features]
    y = df[target]
    model = LogisticRegression(n_jobs=-1)
    rfe = RFE(model, 1)
    fit = rfe.fit(X, y)
    log = dict(zip(features, fit.ranking_))
    log = sorted(log.items(), key=lambda x: abs(x[1]))
    return log

def feature_selection_with_trees(df, features, target):
    X = df[features]
    y = df[target]
    clf = ExtraTreesClassifier(n_jobs=-1)
    clf = clf.fit(X, y)
    clflist = list(clf.feature_importances_)
    draw_feature_importance(clf.feature_importances_, features, "Trees", 3)
    mapped = dict(zip(features, clflist))
    mapped = sorted(mapped.items(), key=lambda x: abs(x[1]), reverse=True)
    return dict(mapped)

def feature_selection_with_linear(df, features, target):
    X = df[features]
    y = df[target]
    lm = LinearRegression(n_jobs=-1)
    lm.fit(X, y)
    print("b0 = {}".format(lm.intercept_))
    draw_feature_importance(lm.coef_, features, "Linear Regression", 4)
    ln = dict(zip(features, lm.coef_))
    print(ln)
    ln = sorted(ln.items(), key=lambda x: abs(x[1]), reverse=True)
    return dict(ln)

def feature_selection_with_forest(df, features, target):
    X = df[features]
    y = df[target]
    forest = RandomForestClassifier(n_jobs=-1)
    forest.fit(X, y)
    featlist = list(forest.feature_importances_)
    draw_feature_importance(forest.feature_importances_, features, "Random Forest", 5)
    forst = dict(zip(features, featlist))
    forst = sorted(forst.items(), key=lambda x: abs(x[1]), reverse=True)
    return dict(forst)

def feature_selection_with_ttest(df, features, target):
    plist = {}
    pvaluelist = []
    for s in features:
        truedata = df[df[target] == True][s]
        falsedata = df[df[target] == False][s]
        _, pvalue = stats.ttest_ind(truedata, falsedata)
        pvaluelist.append(pvalue)
        plist[s] = pvalue
    plist = sorted(plist.items(), key=lambda x: abs(x[1]))
    return dict(plist)

def feature_selection_with_univariate(df, features, target, feature_count):
    X = df[features]
    y = df[target]
    test = SelectKBest(score_func=chi2, k=feature_count)
    fit = test.fit(X, y)
    uni = dict(zip(X.columns, fit.scores_))
    uni = sorted(uni.items(), key=lambda x: abs(x[1]), reverse=True)
    return dict(uni)

def feature_selection_with_variance_threshold(df, features, target, threshold):
    try:
        X = df[features]
        sel = VarianceThreshold(threshold=(threshold * (1 - threshold)))
        sel.fit_transform(X)
        feat = X[[c for (s, c) in zip(sel.get_support(), X.columns.values) if s]]
        finalDf = pd.concat([feat, df[[target]]], axis=1)
        return finalDf
    except Exception:
        return df

def merge_feature_selection_algorithms(data_normalized, columns_without_target, target, feature_count):
    length = len(columns_without_target)
    best_features = {}
    features_from_algorithms = []
    for features in columns_without_target:
        best_features[features] = 0

    def calculate_feature_imp(dic):
        for i in range(0, length):
            for j in range(0, length):
                if list(dic)[i] == list(best_features)[j]:
                    best_features[list(best_features)[j]] += i

    feat_tree = feature_selection_with_trees(data_normalized, columns_without_target, target)
    feat_linear = feature_selection_with_linear(data_normalized, columns_without_target, target)
    feat_log = feature_selection_with_logistic(data_normalized, columns_without_target, target)
    feat_ttest = feature_selection_with_ttest(data_normalized, columns_without_target, target)
    feat_score = feature_selection_with_univariate(data_normalized, columns_without_target, target, feature_count)
    feat_forest = feature_selection_with_forest(data_normalized, columns_without_target, target)

    calculate_feature_imp(feat_tree)
    calculate_feature_imp(feat_linear)
    calculate_feature_imp(feat_score)
    calculate_feature_imp(feat_ttest)
    calculate_feature_imp(feat_log)
    calculate_feature_imp(feat_forest)
    sorted_best_features = sorted(best_features.items(), key=lambda x: abs(x[1]))
    for i in range(0, feature_count):
        features_from_algorithms.append(sorted_best_features[i][0])
    min_feature = features_from_algorithms.copy()
    min_feature.append(target)
    data_normalized_with_min_feature = data_normalized.filter(min_feature, axis=1)
    return data_normalized_with_min_feature, features_from_algorithms

def feature_selection_with_PCA(df, features, value):
    X = df[features]
    data_scaled = pd.DataFrame(data=preprocessing.scale(X), columns=features)
    pca = PCA(n_components=value, svd_solver='full')
    pca.fit_transform(data_scaled)
    explanedvariance = pca.explained_variance_ratio_.cumsum()
    return explanedvariance

def feature_selection_with_PCA_Df(df, features, target, componentvalue):
    X = df[features]
    pca = PCA(n_components=componentvalue)
    principalComponents = pca.fit_transform(X)
    principalDf = pd.DataFrame(data=principalComponents)
    finalDf = pd.concat([principalDf, df[[target]]], axis=1)
    percent_variance = np.round(pca.explained_variance_ratio_ * 100, decimals=2)
    columns = []
    for componentname in range(1, componentvalue+1):
        col = "PC"+ str(componentname)
        columns.append(col)
    plt.figure(6)
    rn = range(1, componentvalue+1)
    plt.bar(x=rn, height=percent_variance, tick_label=columns, color="r")
    plt.ylabel('Percentage of Variance Explained')
    plt.xlabel('Principal Component')
    plt.title('PCA Scree Plot')
    plt.savefig(basepath+"/Plots/PCA Scree Plot.png")
    return finalDf

def correlation_thresholds(df, thresh):
    corrMatrix = df.corr()
    corrMatrix.loc[:, :] = np.tril(corrMatrix, k=-1)
    already_in = set()
    result = []
    for col in corrMatrix:
        perfect_corr = corrMatrix[col][corrMatrix[col] > thresh].index.tolist()
        if perfect_corr and col not in already_in:
            already_in.update(set(perfect_corr))
            perfect_corr.append(col)
            result.append(perfect_corr)
    select_nested = [f[1:] for f in result]
    select_flat = [i for j in select_nested for i in j]
    return select_flat

def calculate_lift(pred, pred_proba, y_test, algorithm, fig):
    pred_df = pd.DataFrame(pred)
    predicted_df = pd.DataFrame(pred_proba)
    y_test_r = y_test.reset_index()
    predictions = pd.concat([y_test_r, pred_df, predicted_df], axis=1)
    predictions.columns = ['index', 'actual', 'predicted', 'falserate', 'truerate']
    predictions.actual = predictions.actual.map({True: 1.0, False: 0.0})
    predictions.predicted = predictions.predicted.map({True: 1.0, False: 0.0})
    predictions.sort_values(by=['truerate'], ascending=False, inplace=True)
    predictions.reset_index(inplace=True)
    predictions['decile'] = pd.qcut(predictions.index, 10, labels=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])
    predictions.drop(['level_0'], axis=1, inplace=True)
    newpredictions = predictions.loc[(predictions.decile == "1") | (predictions.decile == "2")]
    #y= newpredictions.actual
    #p =newpredictions.predicted
    rowcount = newpredictions.shape[0]
    true = 0
    for i in range(rowcount):
        if (newpredictions.actual[i] == 1.0) and (newpredictions.predicted[i] == 1.0):
            true += 1
    percentoftwenty = 100 * float(true) / float(rowcount)
    plt.figure(fig)
    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 8))
    ax1.set_title('Cumulative Gain Chart')
    skplt.metrics.plot_cumulative_gain(y_test, pred_proba, ax=ax1)
    ax2.set_title('Lift Curve')
    skplt.metrics.plot_lift_curve(y_test, pred_proba, ax=ax2)
    f.savefig(basepath+"/Plots/Cumulative Gain - Lift for {}.png".format(algorithm))
    return float(percentoftwenty/20)
    #return lift_score(y, p)

def find_best_parameters(X_train, y_train, algorithm=None):
    parameter = dict()
    estimator = None
    if algorithm == "rf":
        parameters = {
            'n_estimators'      : [int(x) for x in np.arange(100, 250, 50)],
            'max_depth'         : [int(x) for x in np.arange(3, 30)],
            'bootstrap'         : [True, False],
            #n_estimators'      : [int(x) for x in np.linspace(start=10, stop=200, num=10)],
        }
        parameter = parameters
        estimator = RandomForestClassifier()

    if algorithm == "dt":
        parameters = {
            'max_depth'         : [int(x) for x in np.arange(2, 30)]
        }
        parameter = parameters
        estimator = DecisionTreeClassifier()

    if algorithm == "lda":
        parameters = {
            'n_components'      : [None, 1]
        }
        parameter = parameters
        estimator = LinearDiscriminantAnalysis()

    if algorithm == "knn":
        parameters = {
            'n_neighbors'       : [int(x) for x in np.arange(2, 30)]
        }
        parameter = parameters
        estimator = KNeighborsClassifier()

    if algorithm == "nn":
        parameters = {
            'alpha'             : [1e-5, 1e-6],
            'max_iter'          : [int(x) for x in np.arange(100, 201, 100)],
            'hidden_layer_sizes': [int(x) for x in np.around(np.geomspace(4, 256, num=7)).astype(int)]
        }
        parameter = parameters
        estimator = MLPClassifier()

    if algorithm != None:
        try:
            clf = GridSearchCV(estimator=estimator,
                               param_grid=parameter,
                               scoring={
                                   'AUC': 'roc_auc',
                                   'F1 Score': 'f1',
                                   'Accuracy': make_scorer(accuracy_score),
                                   'MCC': make_scorer(matthews_corrcoef)
                               },
                               verbose=2,
                               cv=5,
                               refit='AUC',
                               n_jobs=-1)
        except Exception:
            clf = GridSearchCV(estimator=estimator,
                               param_grid=parameter,
                               cv=5,
                               scoring={
                                   'Accuracy': make_scorer(accuracy_score),
                                   'MCC': make_scorer(matthews_corrcoef)
                               },
                               verbose=2,
                               refit='MCC',
                               n_jobs=-1)
        clf.fit(X_train, y_train)
        with open(basepath+'/ML_params/{}_bestparams.txt'.format(algorithm), 'w') as file:
            file.write(json.dumps(clf.best_params_))
        return clf.best_params_


def run_the_algorithm(function, algorithm_name, algorithm_code, X_test, X_train, y_train, y_test):
    algorithm_figure = {
        'GaussianNaiveBayes':7,
        'SupportVectorMachine':8,
        'LogisticRegression':9,
        'KNN':10,
        'LinearDiscriminantAnalysis':11,
        'DecisionTree':12,
        'NeuralNetwork':13,
        'RandomForest':14,
        'LinearRegression':15
    }
    function.fit(X_train, y_train)
    pred = function.predict(X_test)
    trainacc = function.score(X_train, y_train)
    testacc = function.score(X_test, y_test)
    try:
        f1 = f1_score(y_test, pred, average='micro')
    except Exception:
        f1 = f1_score(y_test, pred, average='macro')
    try:
        mcc = matthews_corrcoef(y_test, pred)
    except Exception:
        mcc = 0
    #roc_auc = draw_roc_curve_micro(function, algorithm_name, X_test, y_test)
    roc_auc = draw_roc_curve_macro(algorithm_name, y_test, pred)
    try:
        draw_conf_matrix(y_test, pred, algorithm_name, algorithm_figure.get(algorithm_name))
    except Exception:
        pass
    values = {"Classifier": algorithm_name, "Train Accuracy": trainacc, "Test Accuracy": testacc, "MCC": mcc, "AUC": roc_auc}
    algorithm_df = pd.DataFrame(values, index=[algorithm_code])
    with open(basepath+'/Models/{}.pkl'.format(algorithm_name), 'wb') as file:
        pickle.dump(function, file)

    return algorithm_df

def give_me_best_algorithm(Data, columns_without_target, target, OUTLIER=False, SCALING=False, FEATURE_SELECTION_COUNT=None, USE_PCA=False):
    Data = data_manipulation(Data, OUTLIER=OUTLIER,SCALING=SCALING) #manipulation
    draw_corr_matrix(Data)
    if FEATURE_SELECTION_COUNT is not None:
        Data_with_min_feature, features_from_algorithm = merge_feature_selection_algorithms(Data, columns_without_target, target, FEATURE_SELECTION_COUNT)
        print("Selected Features: {}".format(features_from_algorithm))
        X_algorithm = Data_with_min_feature[features_from_algorithm]
        y_algorithm = Data_with_min_feature[target]
        if USE_PCA:
            var_PCA = feature_selection_with_PCA(Data_with_min_feature, features_from_algorithm, 0.80)
            df_PCA = feature_selection_with_PCA_Df(Data_with_min_feature, features_from_algorithm, target, len(var_PCA))
            PCA_features = []
            for fea in range(len(var_PCA)):
                PCA_features.append(fea)
            X = df_PCA[PCA_features]
            y = df_PCA[target]
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=SEED)
            print("Selected Features Count with PCA: {}".format(len(var_PCA)))
        else:
            X_train, X_test, y_train, y_test = train_test_split(X_algorithm, y_algorithm, test_size=0.3, random_state=SEED)
    else:
        X = Data[columns_without_target]
        y = Data[target]
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=SEED)

    best_params_lda = find_best_parameters(X_train, y_train, "lda")
    lin = run_the_algorithm(LinearDiscriminantAnalysis(n_components=best_params_lda['n_components']), "LinearDiscriminantAnalysis", "lin", X_test, X_train, y_train, y_test)

    best_params_knn = find_best_parameters(X_train, y_train, "knn")
    kn = run_the_algorithm(KNeighborsClassifier(n_neighbors=best_params_knn['n_neighbors'], n_jobs=-1), "KNN", "k", X_test, X_train, y_train, y_test)

    best_params_dt = find_best_parameters(X_train, y_train, "dt")
    dt = run_the_algorithm(DecisionTreeClassifier(max_depth=best_params_dt['max_depth']), "DecisionTree", "dt", X_test, X_train, y_train, y_test)

    best_params_rf = find_best_parameters(X_train, y_train, "rf")
    forest = run_the_algorithm(RandomForestClassifier(max_depth=best_params_rf['max_depth'], n_jobs=-1, bootstrap=best_params_rf['bootstrap'], n_estimators=best_params_rf['n_estimators']), "RandomForest", "rf", X_test, X_train, y_train, y_test)

    #best_params_nn = find_best_parameters(X_train, y_train, "nn")
    #neural = run_the_algorithm(MLPClassifier(alpha=best_params_nn['alpha'], hidden_layer_sizes=(best_params_nn['hidden_layer_sizes'],),max_iter=best_params_nn['max_iter']), "NeuralNetwork", "neu", X_test, X_train, y_train, y_test)

    #linReg = run_the_algorithm(LinearRegression(), "LinearRegression", "linReg", X_test, X_train, y_train, y_test)
    gauss = run_the_algorithm(GaussianNB(), "GaussianNaiveBayes", "gauss", X_test, X_train, y_train, y_test)
    supp = run_the_algorithm(SVC(probability=True), "SupportVectorMachine", "supp", X_test, X_train, y_train, y_test)
    log = run_the_algorithm(LogisticRegression(n_jobs=-1), "LogisticRegression", "log", X_test, X_train, y_train, y_test)

    #if FEATURE_SELECTION_COUNT is not None:
        #Data_with_min_feature.to_csv("transformed_data.csv", index=False)
    acc_of_algorithms = pd.concat([dt,
                                   log,
                                   kn,
                                   lin,
                                   gauss,
                                   supp,
                                   #neural,
                                   forest,
                                   #linReg
                                   ])
    if acc_of_algorithms['AUC'][0] != 0:
        plt.tight_layout()
        rocplot = plt.figure(2, figsize=(10, 8))
        rocplot.savefig(basepath+"/Plots/ROC Curve.png")
    acc_of_algorithms['Best Value'] = acc_of_algorithms.apply(lambda row: row.MCC**2 + row.AUC**2, axis=1)

    acc_of_algorithms.sort_values(by=['Best Value'], inplace=True, ascending=False)
    return acc_of_algorithms, acc_of_algorithms.iloc[0].tolist()[0]
