# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
import warnings
import pandas as pd
import Functions as Func
warnings.filterwarnings("ignore")

COLUMNS_WITHOUT_TARGET = ['satisfaction_level', 'last_evaluation', 'number_project', 'average_montly_hours', 'time_spend_company', 'Work_accident', 'promotion_last_5years', 'dept', 'salary']
TARGET = 'left'
HR_DATA = pd.read_csv('HR-dataset.csv')
FEATURE_SELECTION_COUNT = 9
ACC_OF_ALGORITHMS, BEST_ALGORITHM = Func.give_me_best_algorithm(HR_DATA, COLUMNS_WITHOUT_TARGET, TARGET, 
                                            OUTLIER=False, SCALING=False, FEATURE_SELECTION_COUNT=None, USE_PCA=False)
print(ACC_OF_ALGORITHMS)
print(BEST_ALGORITHM)
